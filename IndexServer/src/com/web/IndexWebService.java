package com.web;

import java.util.List;

public class IndexWebService {
	private mysqlConnection con;

	public IndexWebService() {
		this.con = new mysqlConnection();
	}

	public void register(String username, String name, String ip, int visibility, int status) {
		con.register(username, name, ip, visibility, status);
	}

	public void unregister(String username) {
		con.unregister(username);
	}

	public void setStatusOffline(String username) {
		con.setStatusOffline(username);
	}
	
	public String getFriendsFromSearch(String keyword) {
		return con.getFriendsFromSearch(keyword);
	}
	
	public String getIP(String username) {
		return con.getIP(username);
	}
	
	public String getName(String username) {
		return con.getName(username);
	}

	public int getVisibility(String username) {
		return con.getVisibility(username);
	}
	
	public int getOnline(String username) {
		return con.getOnline(username);
	}

}