package com.web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class mysqlConnection {
//  private Calendar date= Calendar.getInstance();
    
   private Connection conn = null;
   
   //SELECT//
   private PreparedStatement psSelectRecord=null;
   private ResultSet rsSelectRecord=null;
   private String sqlSelectRecord=null;
   
   //UPDATE//
   private PreparedStatement psUpdateRecord=null;
   private String sqlUpdateRecord=null;
   
   //INSERT//
   private PreparedStatement psInsertRecord=null;
   private String sqlInsertRecord=null;
   
   //DELETE//
   private PreparedStatement psDeleteRecord=null;
   private String sqlDeleteRecord=null;
   
   public mysqlConnection()
   {
       try
       {
         
           String userName = "manager";
           String pass = "password";
           String url = "jdbc:mysql://192.168.1.10:8889/socialnetwork";
//           String url = "jdbc:mysql://db8.papaki.gr:3306/socialnetwork";
           Class.forName("com.mysql.jdbc.Driver").newInstance();
           conn = DriverManager.getConnection (url, userName, pass);
           //System.out.println ("|============Connection with Database established===========|"+"</br>");
       } catch (Exception e) {
           System.out.println("Exception :" + e);}

   } 
   
   public void disconnect()
   {
       try
           {
               //SELECT//
               if(psSelectRecord!=null)
               {
                   psSelectRecord.close();
               }
               if(rsSelectRecord!=null)
               {
                   rsSelectRecord.close();
               }
               
               //UPDATE//
               if(psUpdateRecord!=null)
               {
                   psUpdateRecord.close();
               }   
               
               //INSERT//
               if(psInsertRecord!=null)
               {
                   psInsertRecord.close();
               }
               
               //DELETE//
               if(psDeleteRecord!=null)
               {
                   psDeleteRecord.close();
               }
               
               //GENERAL//
               if(conn!=null)
               {
                   conn.close();
               }
           }
           catch(Exception e)
           {
               e.printStackTrace(); 
               
           }
   }
   
   public void register(String username, String name, String ip, int visible, int status){
	   try
       {
		   
		   sqlUpdateRecord="UPDATE index_server SET ip=?, name=?, visible=?, status=? WHERE username=?";
           psUpdateRecord=conn.prepareStatement(sqlUpdateRecord);

           psUpdateRecord.setString(1,ip);
           psUpdateRecord.setString(2,name);
           psUpdateRecord.setInt(3,visible);
           psUpdateRecord.setInt(4,status);
           psUpdateRecord.setString(5,username);
           

         if(  psUpdateRecord.executeUpdate()==0){
        	 
        	 sqlInsertRecord="insert into index_server (username, name, ip, visible, status) values(?,?,?,?,?)";
        	 psInsertRecord=conn.prepareStatement(sqlInsertRecord);
        	 psInsertRecord.setString(1,username);
        	 psInsertRecord.setString(2,name);
        	 psInsertRecord.setString(3,ip);
        	 psInsertRecord.setInt(4,visible);              
        	 psInsertRecord.setInt(5,status);
        	 
        	 psInsertRecord.executeUpdate();
        	 
         }
           
           
           
       }
       catch(Exception e)
       {
    	   System.out.println("Register Exception :" + e);
       }
       
   
   }
   
   public void unregister(String username){
	   try
       {
           sqlDeleteRecord="Delete from index_server where username=?";
           psDeleteRecord=conn.prepareStatement(sqlDeleteRecord);
           psDeleteRecord.setString(1,username);
           
           psDeleteRecord.executeUpdate();
       }
       catch(Exception e)
       {
        System.out.println("Exception :" + e);
       }
   }
   
   public String getIP(String username){
	   System.out.println("getting ip for: " + username );
	   String ip = null;
	   sqlSelectRecord ="SELECT ip FROM index_server WHERE username=?";
       try
       {
           psSelectRecord=conn.prepareStatement(sqlSelectRecord);
           psSelectRecord.setString(1,username);

           rsSelectRecord=psSelectRecord.executeQuery();
           
           if(rsSelectRecord.next()) {
        	  	ip =  rsSelectRecord.getString(1);
           }
       }catch (Exception e) {
           System.out.println("Exception :" + e);

       }
       
       return ip;
   }
   
   public String getName(String username){
	   System.out.println("getting name for: " + username );
	   String name = null;
	   sqlSelectRecord ="SELECT name FROM index_server WHERE username=?";
       try
       {
           psSelectRecord=conn.prepareStatement(sqlSelectRecord);
           psSelectRecord.setString(1,username);

           rsSelectRecord=psSelectRecord.executeQuery();
           
           if(rsSelectRecord.next()) {
        	  	name =  rsSelectRecord.getString(1);
           }
       }catch (Exception e) {
           System.out.println("Exception :" + e);

       }
       
       return name;
   }
   
   public int getOnline(String username){
	   System.out.println("getting online status for: " + username );
	   int status = 0;
	   
	   sqlSelectRecord ="SELECT status FROM index_server WHERE username=?";
       try
       {
           psSelectRecord=conn.prepareStatement(sqlSelectRecord);
           psSelectRecord.setString(1,username);

           rsSelectRecord=psSelectRecord.executeQuery();
           
           if(rsSelectRecord.next()) {
        	  	status =  rsSelectRecord.getInt(1);
           }
       }catch (Exception e) {
           System.out.println("Exception :" + e);

       }
       
       return status;
	   
   }
   
   public int getVisibility(String username){
	   int visible = 0;
	   
	   sqlSelectRecord ="SELECT visible FROM index_server WHERE username=?";
       try
       {
           psSelectRecord=conn.prepareStatement(sqlSelectRecord);
           psSelectRecord.setString(1,username);

           rsSelectRecord=psSelectRecord.executeQuery();
           
           while(rsSelectRecord.next()) {
        	   visible = rsSelectRecord.getInt(1);
           }
       }catch (Exception e) {
           System.out.println("Exception :" + e);

       }
       
       return visible;
   }

    public void setStatusOffline(String username) {
    	try
        {
 		   	sqlUpdateRecord="UPDATE index_server SET status=? WHERE username=?";
            psUpdateRecord=conn.prepareStatement(sqlUpdateRecord);

            psUpdateRecord.setInt(1,0);
            psUpdateRecord.setString(2,username);
            
            psUpdateRecord.executeUpdate();
        }catch (Exception e) {
            System.out.println("Exception :" + e);

        }
	
    }

    public String getFriendsFromSearch(String keyword) {
    	String retList = "";
    	
    	sqlSelectRecord ="SELECT username, name FROM index_server WHERE visible=? AND (name LIKE ? OR username LIKE ?)";
    	try
        {
            psSelectRecord=conn.prepareStatement(sqlSelectRecord);
            psSelectRecord.setInt(1,1);
            psSelectRecord.setString(2,"%"+keyword+"%");
            psSelectRecord.setString(3,"%"+keyword+"%");
            

            rsSelectRecord=psSelectRecord.executeQuery();
            
            while(rsSelectRecord.next()) {
            	retList += rsSelectRecord.getString(1) + ":" + rsSelectRecord.getString(2) + "-";
            }
        }catch (Exception e) {
            System.out.println("Exception :" + e);

        }
		return retList;
    	
	
    }    
    
//    public List<Friend> getFriendsFromSearch() {
//    	List<Friend> retList = new ArrayList<Friend>();
//    	
//    	sqlSelectRecord ="SELECT username, name FROM index_server WHERE visible=?";
//    	try
//        {
//            psSelectRecord=conn.prepareStatement(sqlSelectRecord);
//            psSelectRecord.setInt(1,1);
//
//            rsSelectRecord=psSelectRecord.executeQuery();
//            
//            while(rsSelectRecord.next()) {
//         	  Friend friend = new Friend();
//         	  friend.setUsername(rsSelectRecord.getString(1));
//         	  friend.setName(rsSelectRecord.getString(2));
//         	  retList.add(friend);
//            }
//        }catch (Exception e) {
//            System.out.println("Exception :" + e);
//
//        }
//		return retList;
//    	
//	
//    }
 }
